export const state = () => {
    return {
        user: null,
    };
};

export const mutations = {
    updateState(state, data) {
        Object.assign(state, data);
    },
};

export const getters = {};

export const actions = {};

export default {
    namespaced: true,
    state,
    mutations,
    getters,
    actions,
};
